#!/usr/bin/bash

# Read doc.txt for shell color escape codes

dir_of_script=$(dirname $0)

source ${dir_of_script}/color_def.sh

function log_info()
{
    echo -e "\e[${color_light_green}[INFO]\e[0m $1"
}

function log_warn()
{
    echo -e "\e[${color_light_yellow}[WARN]\e[0m $1"
}

function log_err()
{
    echo -e "\e[${color_red}[ERROR]\e[0m $1"
}
