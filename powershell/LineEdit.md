# Back/Forward a character

L, R arrow

# Back/Forward a word

`<ctr>` + (L, R) arrow

# Start/End of the line

`<home> / <end>` key
